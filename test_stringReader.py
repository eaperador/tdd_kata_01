from unittest import TestCase
from StringReader import *

class TestStringReader(TestCase):
    def test_countElements(self):
        self.assertEqual(StringReader().countElements(""), 0, "String con cadena vacia")
        self.assertEqual(StringReader().countElements("1"), 1, "String con cadena de un numero")
        self.assertEqual(StringReader().countElements("1,2"), 2, "String con cadena de dos numero")
        self.assertEqual(StringReader().countElements("1,2,3,4"), 4, "String con cadena de n numeros")
    def test_countMinimoElements(self):
        self.assertEqual(StringReader().countMinimoElements("")[0], 0, "Paso 2: contar String con cadena vacia")
        self.assertEqual(StringReader().countMinimoElements("")[1], "Vacio", "Paso 2: minimo con cadena vacia")
        self.assertEqual(StringReader().countMinimoElements("1")[0], 1, "Paso 2: contar String con un numero")
        self.assertEqual(StringReader().countMinimoElements("1")[1], 1, "Paso 2: minimo con un numero")
        self.assertEqual(StringReader().countMinimoElements("1,2")[0], 2, "Paso 2: contar String con dos numero")
        self.assertEqual(StringReader().countMinimoElements("1,2")[1], 2, "Paso 2: minimo con dos numero")
        self.assertEqual(StringReader().countMinimoElements("1,2,3,4")[0], 2, "Paso 2: contar String con n numero")
        self.assertEqual(StringReader().countMinimoElements("1,2,3,4")[1], 2, "Paso 2: minimo con n numero")
    def test_countMinimoMaximoElements(self):
        self.assertEqual(StringReader().countMinimoMaximoElements("")[0], 0, "Paso 3: contar String con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoElements("")[1], "Vacio", "Paso 3: minimo con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoElements("")[2], "Vacio", "Paso 3: maximo con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoElements("1")[0], 1, "Paso 3: contar String con un numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1")[1], 1, "Paso 3: minimo con un numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1")[2], 1, "Paso 3: maximo con un numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2")[0], 2, "Paso 3: contar String con dos numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2")[1], 1, "Paso 3: minimo con dos numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2")[2], 2, "Paso 3: maximo con dos numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2,3,4")[0], 4, "Paso 3: contar String con n numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2,3,4")[1], 1, "Paso 3: minimo con n numero")
        self.assertEqual(StringReader().countMinimoMaximoElements("1,2,3,4")[2], 4, "Paso 3: maximo con n numero")
    def test_countMinimoMaximoPromedioElements(self):
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("")[0], 0, "Paso 4: contar String con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("")[1], "Vacio", "Paso 4: minimo con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("")[2], "Vacio", "Paso 4: maximo con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("")[3], "Vacio", "Paso 4: promedio con cadena vacia")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1")[0], 1,
                         "Paso 4: contar String con un numero")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1")[1], 1,
                         "Paso 4: minimo con cadena un numero")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1")[2], 1,
                         "Paso 4: maximo con cadena un numero")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1")[3], 1,
                         "Paso 4: promedio con cadena un numero")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2")[0], 2,
                         "Paso 4: contar String con dos numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2")[1], 1,
                         "Paso 4: minimo con cadena dos numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2")[2], 2,
                         "Paso 4: maximo con cadena dos numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2")[3], 1.5,
                         "Paso 4: promedio con cadena dos numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2,3")[0], 3,
                         "Paso 4: contar String con n numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2,3")[1], 1,
                         "Paso 4: minimo con cadena n numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2,3")[2], 3,
                         "Paso 4: maximo con cadena n numeros")
        self.assertEqual(StringReader().countMinimoMaximoPromedioElements("1,2,3")[3], 2,
                         "Paso 4: promedio con cadena n numeros")
