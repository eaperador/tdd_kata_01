__author__="e.aperador@uniandes.edu.co ,av.carranza@uniandes.edu.co"

class StringReader:
    def countElements(self, cadena):
        if(cadena == ""):
            return 0
        return len(cadena.split(","))
    def countMinimoElements(self, cadena):
        if(cadena == ""):
            return (0,"Vacio")
        return (len(cadena.split(",")), int(min(cadena.split(","))))
    def countMinimoMaximoElements(self, cadena):
        if (cadena == ""):
            return (0,"Vacio","Vacio")
        return (len(cadena.split(",")), int(min(cadena.split(","))),int(max(cadena.split(","))))

    def countMinimoMaximoPromedioElements(self, cadena):
        if (cadena == ""):
            return (0,"Vacio","Vacio", "Vacio")
        arreglo = cadena.split(",")
        promedio = 0;
        for valor in arreglo:
            promedio += int(valor)
        promedio = promedio/len(arreglo)
        return (len(arreglo), int(min(arreglo)), int(max(arreglo)), promedio)